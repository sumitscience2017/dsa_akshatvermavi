class Solution {
public:
   void solveusingbackt(vector<int>& nums, vector<vector<int>>& ans, int start){
        if(start<nums.size())
        {
            ans.push_back(nums);
        }
        //map<int, bool> visited; this is giving error
        for(int i =start;i<nums.size();i++)
        {
            // if(visited.find(nums[i])!=visited.end());
            // continue;

            // visited[nums[i]] = true;
           
            swap(nums[i],nums[start]);
            solveusingbackt(nums, ans, start+1);
            swap(nums[i], nums[start]);
        }
    }
    vector<vector<int>> permuteUnique(vector<int>& nums) {
      vector<vector<int>> ans;
      //sort(nums.begin(),nums.end());
      solveusingbackt(nums, ans, 0);
      set<vector<int>>st;
      for(auto i: ans){
          st.insert(i);
      }
      ans.clear();
      for(auto i: st){
          ans.push_back(i);
      }
      return ans;  
    }
};
